#!/bin/bash
arm-vita-eabi-gcc -Wl,-q -O3 -x c ../../test.h -I../../.. -c -o test.o -DSAF_PLATFORM_VITA
arm-vita-eabi-gcc -Wl,-q -O3 test.o -o test.elf -lSceDisplay_stub -lSceCtrl_stub -lSceAudio_stub
vita-elf-create test.elf test.velf
vita-make-fself -s test.velf eboot.bin
vita-mksfoex -s TITLE_ID="SAFTEST01" "SAF test" param.sfo
vita-pack-vpk -s param.sfo -b eboot.bin test.vpk
