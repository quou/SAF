#!/bin/bash
arm-vita-eabi-gcc -Wl,-q -O3 -x c ../../utd.h -I../../.. -c -o utd.o -DSAF_PLATFORM_VITA
arm-vita-eabi-gcc -Wl,-q -O3 utd.o -o utd.elf -lSceDisplay_stub -lSceCtrl_stub -lSceAudio_stub
vita-elf-create utd.elf utd.velf
vita-make-fself -s utd.velf eboot.bin
vita-mksfoex -s TITLE_ID="SAFUTD001" "UTD" param.sfo
vita-pack-vpk -s param.sfo -b eboot.bin utd.vpk \
	-a sce_sys/icon0.png=sce_sys/icon0.png
